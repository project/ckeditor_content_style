INTRODUCTION
--------------------------------------------------------------------------------

This is not Grammar. This is not spell checker.
This plugin adds option for site admin of a website to control content quality.
Site Admin can create catalogue of words with entity.
And that must be followed by content writer.

Example use cases.
In a news organization, chief content writer can create its own catalogue.
And ask all of content writer to follow same catalogue.
So whenever content writer create content,
They check their article content agaist catalogue.

E.g.
Whenever content writer address president of united state in their article.
And when they check their article content by content style guide.
They are suggested to use,
words The president of united state instead of just president in their article.
The president of united state Would be part of catalogue,
created by chief content writer/site admin.

REQUIREMENTS
------------

- ckeditor
- editor
  
INSTALLATION
------------

Install as usual, see
https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8
for further information.

CONFIGURATION
------------

1. Login as administrator.
3. Configure your WYSIWYG toolbar to include the buttons.
4. Go to /admin/ckcs
5. Add your entity and suspected words with suggestion.

MAINTAINERS
-----------

Current maintainers and owner:

 * Md Meraj Ahmed (https://www.drupal.org/user/3503902/)
